<?php

namespace App\Controller;

use App\Repository\NeoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/neo")
 */
class NeoController extends AbstractController
{
    private NeoRepository $neoRepository;
    
    public function __construct(NeoRepository $neoRepository)
    {
        $this->neoRepository = $neoRepository;
    }
    
    /**
     * @Route("/hazardous", methods={"GET"})
     */
    public function getHazardous(Request $request): JsonResponse
    {
        $page = $request->query->get('page') ?? 1;
        $hazardous = $this->neoRepository->findHazardous($page);
        
        return $this->json($hazardous, 200);
    }
    
    /**
     * @Route("/fastest", methods={"GET"})
     */
    public function getFastest(Request $request): JsonResponse
    {
        $isHazardous = $this->getIsHazardous($request);
        $fastestNeo = $this->neoRepository->findFastest($isHazardous);
        
        return new JsonResponse($fastestNeo, 200);
    }
    
    /**
     * @Route("/best-month", methods={"GET"})
     */
    public function getBestMonth(Request $request): JsonResponse
    {
        $isHazardous = $this->getIsHazardous($request);
        $bestMonth = $this->neoRepository->findBestMonth($isHazardous);
        
        return new JsonResponse($bestMonth, 200);
    }
    
    private function getIsHazardous($request): bool
    {
        return $request->query->get('hazardous') === 'true';
    }
    
}
