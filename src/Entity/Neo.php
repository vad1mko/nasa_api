<?php

namespace App\Entity;

use App\Repository\NeoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NeoRepository::class)
 */
class Neo implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="date")
     */
    private \DateTimeInterface $date;

    /**
     * @ORM\Column(type="integer")
     */
    private int $reference;

    /**
     * @ORM\Column(type="string", length=65)
     */
    private string $name;

    /**
     * @ORM\Column(type="float")
     */
    private float $speed;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isHazardous;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getReference(): ?int
    {
        return $this->reference;
    }

    public function setReference(int $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSpeed(): ?float
    {
        return $this->speed;
    }

    public function setSpeed(float $speed): self
    {
        $this->speed = $speed;

        return $this;
    }

    public function getIsHazardous(): ?bool
    {
        return $this->isHazardous;
    }

    public function setIsHazardous(bool $isHazardous): self
    {
        $this->isHazardous = $isHazardous;

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'date' => $this->getDate(),
            'reference' => $this->getReference(),
            'name' => $this->getName(),
            'speed' => $this->getSpeed(),
            'isHazardous' => $this->getIsHazardous(),
        ];
    }
}
