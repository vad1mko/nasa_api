<?php

namespace App\Repository;

use App\Entity\Neo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Neo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Neo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Neo[]    findAll()
 * @method Neo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NeoRepository extends ServiceEntityRepository
{
    private const MAX_RESULTS = 10;
    private EntityManagerInterface $entityManager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct($registry, Neo::class);
    }

    public function add(Neo $neo): void
    {
        $this->entityManager->persist($neo);
    }

    public function save(): void
    {
        $this->entityManager->flush();
    }

    // /**
    //  * @return Neo[] Returns an array of Neo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Neo
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findHazardous(int $page): array
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.isHazardous = true')
            ->setFirstResult(($page - 1) * self::MAX_RESULTS)
            ->setMaxResults(self::MAX_RESULTS)
            ->getQuery()
            ->getArrayResult()
            ;
    }
    
    public function findFastest(bool $isHazardous): array
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.isHazardous = :isHazardaous')
            ->setParameter('isHazardaous', $isHazardous)
            ->orderBy('n.speed', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getArrayResult()
            ;
    }
    
    public function findBestMonth(bool $isHazardous): array
    {
        return $this->createQueryBuilder('n')
            ->select('date_format(n.date, \'%m-%Y\') as date, COUNT(n.name) as count')
            ->andWhere('n.isHazardous = :isHazardaous')
            ->setParameter('isHazardaous', $isHazardous)
            ->groupBy('date')
            ->orderBy('count', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getArrayResult()
            ;
    }
    
}
