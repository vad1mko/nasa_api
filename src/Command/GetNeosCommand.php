<?php

namespace App\Command;

use App\Service\NeoHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GetNeosCommand extends Command
{
    protected static $defaultName = 'neos:receive';

    private const DAYS = 3;

    private NeoHelper $neoHelper;

    public function __construct(NeoHelper $neoHelper, string $name = null)
    {
        parent::__construct($name);
        $this->neoHelper = $neoHelper;
    }

    protected function configure()
    {
        $this->setDescription('Get neo data for the last '.self::DAYS.' days and persist it.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->neoHelper->updateNasaNeoData(self::DAYS - 1);

        $io = new SymfonyStyle($input, $output);
        $io->success('Updated.');

        return Command::SUCCESS;
    }
}
