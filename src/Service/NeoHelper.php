<?php

namespace App\Service;

use App\Entity\Neo;
use App\Repository\NeoRepository;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NeoHelper
{
    private string $nasaApiKey;
    private HttpClientInterface $client;
    private NeoRepository $neoRepository;

    public function __construct(HttpClientInterface $client, NeoRepository $neoRepository, string $nasaApiKey)
    {
        $this->nasaApiKey = $nasaApiKey;
        $this->client = $client;
        $this->neoRepository = $neoRepository;
    }

    public function updateNasaNeoData(int $days = 2): void
    {
        $neoData = $this->getNasaApiNeoData($days);
        $this->updateNeos($neoData["near_earth_objects"]);
    }

    public function getNasaApiNeoData(int $days): array
    {
        $dates = $this->getStartAndEndDates($days);
        $response = $this->client->request(
            'GET',
            "https://api.nasa.gov/neo/rest/v1/feed?start_date={$dates['startDate']}&end_date={$dates['endDate']}&api_key={$this->nasaApiKey}"
        );

        return $response->toArray();
    }

    private function getStartAndEndDates(int $days): array
    {
        $endDate = date('Y-m-d');
        $startDate = date('Y-m-d', strtotime($endDate.' -'.$days.' days'));
        return [
            'startDate' => $startDate,
            'endDate' => $endDate,
        ];
    }

    public function updateNeos(array $neoData): void
    {

        foreach ($neoData as $date => $neoDates) {
            foreach ($neoDates as $neoItem) {
                $neo = $this->neoRepository->findOneBy(['reference' => $neoItem['neo_reference_id']]);

                if (null === $neo) {
                    $neo = new Neo();
                }

                $neo->setDate(new \DateTime($date));
                $neo->setName($neoItem['name']);
                $neo->setReference($neoItem['neo_reference_id']);
                $neo->setSpeed($neoItem['close_approach_data'][0]['relative_velocity']['kilometers_per_hour']);
                $neo->setIsHazardous($neoItem['is_potentially_hazardous_asteroid']);

                $this->neoRepository->add($neo);
            }
        }
        $this->neoRepository->save();
    }
}
