<?php

namespace App\Tests\Controller;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NeoControllerTest extends ApiTestCase
{
    private HttpClientInterface $client;
    
    public function __construct()
    {
        $this->client = static::createClient();
        parent::__construct();
    }
    
    public function testHazardous()
    {
        $this->client->request('GET', '/neo/hazardous');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
    
    public function testFastest()
    {
        $this->client->request('GET', '/neo/fastest');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        
        $this->client->request('GET', '/neo/fastest?hazardous=true');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
    
    public function testBestMonth()
    {
        $this->client->request('GET', '/neo/best-month');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        
        $this->client->request('GET', '/neo/best-month?hazardous=true');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
}
